package org.epam.model;

public class Boeing extends Plane {


    public Boeing(String model, int uploadCapacity, int flightRange, int fuelConsumption, int carryingCapacity) {
        super(model, uploadCapacity, flightRange, fuelConsumption, carryingCapacity);
    }
}

