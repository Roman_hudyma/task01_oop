package org.epam.model;

public abstract class Plane {
    private String model;
    private int uploadCapacity;
    private int flightRange;
    private int fuelConsumption;
    private int carryingCapacity;

    public Plane(String model, int uploadCapacity, int flightRange, int fuelConsumption, int carryingCapacity) {
        this.uploadCapacity = uploadCapacity;
        this.model = model;
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
        this.carryingCapacity = carryingCapacity;
    }



    public void setUploadCapacity(int uploadCapacity) {
        this.uploadCapacity = uploadCapacity;
    }
    public int getUploadCapacity() {
        return uploadCapacity;
    }
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public void setFlightRange(int flightRange) {
        this.flightRange = flightRange;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(int carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
    }

    @Override
    public String toString() {
        return "_______________________________________________________________________________________________________________________________\n" +
                "|Plane|" +
                "uploadCapacity:" + uploadCapacity +
                "||controller:" + model +
                "||flightRange:" + flightRange +
                "||fuelConsumption:" + fuelConsumption +
                "||carryingCapacity:" + carryingCapacity +
                "|";

    }
}
