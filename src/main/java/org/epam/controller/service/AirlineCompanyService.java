package org.epam.controller.service;

import org.epam.model.Plane;

import java.util.List;
import java.util.OptionalDouble;

public interface AirlineCompanyService {
    int sumUploadCapacity();
    int sumCarryingCapacity();
    List<Plane> sortByFlightRange();
    OptionalDouble findByFuelConsurmation();
}
