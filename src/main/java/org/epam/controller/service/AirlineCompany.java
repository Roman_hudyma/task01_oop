package org.epam.controller.service;

import org.epam.model.Plane;

import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class AirlineCompany implements AirlineCompanyService {
    protected List<Plane> airport;


    @Override
    public int sumUploadCapacity() {
        return (int) airport.stream()
                .mapToDouble(Plane::getUploadCapacity)
                .sum();
    }

    @Override
    public int sumCarryingCapacity() {
        return (int) airport.stream()
                .mapToDouble(Plane::getCarryingCapacity)
                .sum();
    }

    @Override
    public List<Plane> sortByFlightRange() {
        return airport.stream()
                .sorted(Comparator.comparing(Plane::getFlightRange))
                .collect(Collectors.toList());
    }
    public OptionalDouble findByFuelConsurmation()
    {

        return airport.stream()
                .mapToDouble(Plane::getFuelConsumption)
                .max();
    }


}
