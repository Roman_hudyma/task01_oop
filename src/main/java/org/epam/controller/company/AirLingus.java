package org.epam.controller.company;

import org.epam.model.Airbus;
import org.epam.model.Basler;
import org.epam.model.Boeing;
import org.epam.controller.service.AirlineCompany;

import java.util.Arrays;

public abstract class AirLingus extends AirlineCompany {
    private static AirlineCompany airLingus;

    {
        airport = Arrays.asList(
                (new Boeing("Boeing 747 LCF Dreamlifter", 40, 2100, 100, 3000)),
                (new Boeing("Boeing 747 LCF Dreamlifter", 70, 1700, 130, 5000)),
                (new Basler("Basler BT-67", 30, 1150, 70, 1100)),
                (new Airbus("Airbus Beluga XL", 50, 1875, 90, 3700))
        );
    }

    public static AirLingus getInstance() {

        if (airLingus == null) {
            return new AirLingus() {
            };
        }
        return (AirLingus) airLingus;

    }
}

