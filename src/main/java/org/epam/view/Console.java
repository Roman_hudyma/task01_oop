package org.epam.view;

import org.epam.controller.company.AirLingus;
import org.epam.controller.service.AirlineCompany;

import java.util.Scanner;

public class Console {
    Scanner scan = new Scanner(System.in);
    AirlineCompany company = new AirLingus() {
    };
    int input;
    double con;

    public void start() {
        while (true) {
            System.out.println(("enter 0 to Show sum and show upload capacity and carrying capacity,\n" +
                    "enter 1 to Show all planes sorted by flight range,\n" +
                    "enter 2 to Show the plane with the largest range of fly,\n" +
                    "enter 4 to QUIT"));
            input = scan.nextInt();
            switch (input) {
                case 0:
                    System.out.println("Sum of upload capacity:" + company.sumUploadCapacity());
                    System.out.println("Sum of carrying capacity:" + company.sumCarryingCapacity());
                    break;

                case 1:
                    System.out.println("List of planes sorted by fuel consumption");
                    company.sortByFlightRange().forEach(System.out::println);
                    break;
                case 2:
                    System.out.println(company.findByFuelConsurmation());
                    break;

                case 3:
                    System.out.println("QUIT");
                    return;
                default:
                    System.out.println("Something goes wrong... try again...");


            }
        }

    }
}

